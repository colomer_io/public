#!/bin/bash
# remove an orphan key (i.e: 1397BC53640DB551)
cd /tmp
gpg --keyserver keys.gnupg.net --recv-key 1397BC53640DB551
gpg --export --armor 1397BC53640DB551 | apt-key add -
# reinstall latest key for ownCloud client repo
wget http://download.opensuse.org/repositories/isv:ownCloud:desktop/Ubuntu_14.04/Release.key
apt-key add - < Release.key
apt update && apt upgrade -y
apt-get autoremove -y
