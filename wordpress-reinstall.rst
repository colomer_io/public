Reinstalar WordPress sin perder contenido
=========================================
Dada la simplicidad estructural de WordPress, en el caso de que el alcance de la infección haya sido importante o hayamos determinado que el sitio ha quedado comprometido, es en muchas ocasiones recomendable realizar a una reinstalación limpia. Es una operación relativamente sencilla que en apenas 20 minutos puede garantizarnos que la instalación está completamente limpia.

Índice
======

1. Actualizar WordPress y plugins
2. Guardado de ficheros necesarios 
3. Eliminar todos los ficheros de WordPress
4. Subir un nuevo WordPress 
5. Recuperar ficheros, plugins y/o temas guardados 
6. Acceder al administrador para cargar la configuración de la base de datos

Pasos a seguir:

1. Actualizar WordPress y plugins
---------------------------------
En primer lugar recomendamos acceder a la copia infectada y actualizar tanto el WordPress como los plugins utilizados a la última versión. Ésta actualización es necesaria para asegurarnos que la estructura de la base de datos y los plugins de WordPress están actualizados a la última versión de WordPress.

2. Reservamos los archivos que consideremos necesarios
------------------------------------------------------
Antes de empezar con la instalación guardaremos copia de los plugins personalizados, comprados, o que no podamos volver a descargar. Comprobaremos especialmente plugins y el tema utilizado en nuestro WordPress. Comprobaremos si estos mantienen una configuración propia dentro de alguna de las siguientes carpetas: /wp-content/plugins, wp-content/themes o wp-content/uploads. Puede resultar útil conservar una copia de los ficheros .htaccess y wp-config.php.

Antes de recuperar el contenido del directorio uploads, verificaremos que no existe ningún archivo .php, a excepción del index.php. 

*Recuerda anotar el listado de los plugins que tenemos actualmente activos*

3. Eliminar todos los ficheros de WordPress
-------------------------------------------
Después de guardar, procedemos a eliminar todos y cada uno de los ficheros del site, incluyendo .htaccess y/o wp-config.php (los hemos guardado en el paso anterior).

Antes de borrarlo, recuerda copiar las credenciales de la base de datos o configuraciones especiales existentes en el fichero wp-config.php. Los datos más relevantes que necesitarás son::

    /** El nombre de tu base de datos de WordPress **/
    define('DB_NAME', 'nombreBBDD');
    /** Tu nombre de usuario de MySQL **/
    define('DB_USER', 'usuarioBBDD');
    /** Tu contraseña de MySQL **/
    define('DB_PASSWORD', 'contraseñaBBDD');

4. Subir una copia limpia de WordPress
--------------------------------------
Sube una copia limpia de los ficheros desde el sitio oficial de WordPress en tu directorio raíz. A continuación deberás añadir las credenciales de la Base de datos en tu nuevo fichero wp-config.php, para cargar los datos con el WordPress y plugins actualizados.

Descarga la última versión desde el sitio oficial de WordPress, recuerda que ésta debe coincidir con la versión actualizada previamente.

5. Recuperar ficheros, plugins y/o temas guardados
--------------------------------------------------
Antes de acceder al wp-admin (importante!) recuperaremos manualmente los ficheros de los plugins que teníamos instalados. Esto lo haremos desde el zip que podemos descargar desde el directorio oficial de plugins de wordpress. En caso excepcional, recuperaremos de la copia salvada aquellos plugins o temas guardados. También recuperaremos las imágenes del directorio uploads, en general cualquier fichero que hayamos tenido que guardar.

Antes de recuperar ningún fichero antiguo, es importante revisar uno por uno su contenido.
Es muy frecuente que estén infectados para poder ser utilizados de caballo de troya.
El directorio uploads, nunca, nunca debería contener archivos .php, a excepción del index.php que estará vacío.

6. Acceder al administrador para cargar la configuración de la base de datos 
----------------------------------------------------------------------------
Una vez tengamos todo recuperados los ficheros (wordpress, plugins y plantilla) desde de copias limpias, y no antes (importante!), procederemos a acceder vía navegador al administrador del site. Al acceder nos mostrará un mensaje de “Actualizar la base de datos”, le decimos que si, para que cargue la configuración guardada (del anterior WordPress) y ya podremos acceder al administrador (dominio.com/wp-admin), con el mismo usuario y contraseña que teníamos previamente.

Para asegurarnos de que todos los plugins se cargan correctamente, desde el /wp-admin, en la pestaña plugins, pulsamos sobre editar y guardar en cada uno de los plugins que queramos activar. Así conseguimos cargar la configuración guardada en la base de datos para cada plugin.

Una vez tengamos completamente recuperado nuestro wordpress, procederemos a securizarlo.

Y con ésto, nos aseguramos de tener una instalación limpia y segura de WordPress
